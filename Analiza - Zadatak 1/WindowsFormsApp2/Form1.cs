﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        protected Graphics g;
        GFX engine;
        ploca ploca;

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics u = panel1.CreateGraphics();
            engine = new GFX(u);
            ploca = new ploca();
            ploca.initploca();
            osvjezi();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            Point mouse = Cursor.Position;
            mouse = panel1.PointToClient(mouse);

            ploca.detectHit(mouse);
            osvjezi();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ploca.Count1 = 0;
            ploca.count2 = 0;
            ploca.reset();
            GFX.setup();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void osvjezi()
        {
            label1.Text = ploca.Count1.ToString();
            label3.Text = ploca.Count2.ToString();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            label2.Text = Interaction.InputBox("", "Ime igrača 1", "", 100, 200) + ": ";
            label4.Text = Interaction.InputBox("", "Ime igrača 2", "", 100, 200) + ": ";
        }
    }

    class ploca
    {
        private Holder[,] holders = new Holder[3, 3];
        public int potez = 0;
        public int partija = 1;
        public int count1 = 0;
        public int count2 = 0;

        public const int X = 0;
        public const int Y = 1;
        public const int B = 2;

        public int Count1
        {
            get
            {
                return count1;
            }
            set
            {
                count1 = value;
            }
        }

        public int Count2
        {
            get
            {
                return count2;
            }
            set
            {
                count2 = value;
            }
        }

        public void initploca()
        {
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    holders[x, y] = new Holder();
                    holders[x, y].Vrijednost = B;
                    holders[x, y].Lokacija = new Point(x, y);
                }
            }
        }

        public void detectHit(Point loc)
        {
            if (loc.Y < 450)
            {
                int x = 0;
                int y = 0;
                if (loc.X < 150)
                {
                    x = 0;
                }
                else if (loc.X > 150 && loc.X < 300)
                {
                    x = 1;
                }
                else
                {
                    x = 2;
                }
                if (loc.Y < 150)
                {
                    y = 0;
                }
                else if (loc.Y > 150 && loc.Y < 300)
                {
                    y = 1;
                }
                else if (loc.Y < 450)
                {
                    y = 2;
                }

                if (partija % 2 == 1)
                {
                    if (potez % 2 == 0)
                    {
                        GFX.drawX(new Point(x, y));
                        holders[x, y].Vrijednost = X;
                        potez++;
                        if (Pobjeda(X))
                        {
                            count1++;
                            reset();
                            GFX.setup();
                            partija++;
                        }
                    }
                    else
                    {
                        GFX.drawO(new Point(x, y));
                        holders[x, y].Vrijednost = Y;
                        potez++;
                        if (Pobjeda(Y))
                        {
                            count2++;
                            reset();
                            GFX.setup();
                            partija++;
                        }
                    }
                }
                else
                {
                    if (potez % 2 == 1)
                    {
                        GFX.drawX(new Point(x, y));
                        holders[x, y].Vrijednost = X;
                        potez++;
                        if (Pobjeda(X))
                        {
                            count1++;
                            reset();
                            GFX.setup();
                            partija++;
                        }
                    }
                    else
                    {
                        GFX.drawO(new Point(x, y));
                        holders[x, y].Vrijednost = Y;
                        potez++;
                        if (Pobjeda(Y))
                        {
                            count2++;
                            reset();
                            GFX.setup();
                            partija++;
                        }
                    }
                }
                if (potez == 9)
                {
                    reset();
                    GFX.setup();
                    partija++;
                }
            }
        }

        public bool Pobjeda(int X)
        {
            bool win = false;
            int x;
            for (x = 0; x < 3; x++)
            {
                if (holders[x, 0].Vrijednost == X && holders[x, 1].Vrijednost == X && holders[x, 2].Vrijednost == X) return true;
                else if (holders[0, x].Vrijednost == X && holders[1, x].Vrijednost == X && holders[2, x].Vrijednost == X) return true;
            }
            x = 0;
            if (holders[x, x].Vrijednost == X && holders[x + 1, x + 1].Vrijednost == X && holders[x + 2, x + 2].Vrijednost == X) return true;
            if (holders[x, x + 2].Vrijednost == X && holders[x + 1, x + 1].Vrijednost == X && holders[x + 2, x].Vrijednost == X) return true;

            return win;
        }

        public void reset()
        {
            holders = new Holder[3, 3];
            initploca();
            potez = 0;
        }
    }



    class Holder
    {
        private Point lokacija;
        private int vrijednost = ploca.B;
        public Point Lokacija
        {
            get
            {
                return lokacija;
            }
            set
            {
                lokacija = value;
            }
        }

        public int Vrijednost
        {
            get
            {
                return vrijednost;
            }
            set
            {
                vrijednost = value;
            }
        }

    }
    class GFX
    {
        private static Graphics gObj;
        public GFX(Graphics g)
        {
            gObj = g;
            setup();
        }

        public static void setup()
        {
            Brush bg = new SolidBrush(Color.Black);
            Pen p = new Pen(Color.White);

            gObj.FillRectangle(bg, 0, 0, 450, 450);
            gObj.DrawLine(p, 150, 0, 150, 450);
            gObj.DrawLine(p, 300, 0, 300, 450);
            gObj.DrawLine(p, 0, 150, 450, 150);
            gObj.DrawLine(p, 0, 300, 450, 300);
        }
        public static void drawX(Point loc)
        {
            Pen pen = new Pen(Color.Yellow, 5F);
            int xA = loc.X * 150;
            int yA = loc.Y * 150;
            gObj.DrawLine(pen, xA + 25, yA + 25, xA + 125, yA + 125);
            gObj.DrawLine(pen, xA + 125, yA + 25, xA + 25, yA + 125);
        }

        public static void drawO(Point loc)
        {
            Pen pen = new Pen(Color.Red, 5F);
            int xA = loc.X * 150;
            int yA = loc.Y * 150;
            gObj.DrawEllipse(pen, xA + 25, yA + 25, 100, 100);
        }
    }
}
